# New logger project from scratch. The project aims to
# Setup a raspberry Pi, with a PiFace and a connected
# temperature and pressure sensor. In this case a BMP280

#Imports
import threading  #Using threads
import socket     #For setting up sockets
import logging    #For the debug logging functions
import sys        #for exit etc
import time       # for sleep function and time since epoch
import datetime   # for nicer time formats
import BMP280_2   #Setting up and communication of teh BMP280 sensor (T, P)

import pifacecommon.core       #Specific for the piface interrupts setup
import pifacecommon.interrupts #for interrupts setup
import pifacedigitalio         #for interrups setup
import os                      #for interrupts setup

import interrupt


#Initial setups before starting main?
################ Logger setup ##########################
#Creating a new logger
logger = logging.getLogger('__logger__')

#Settings for the specific logger created above
#Creates log for this .py file
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s  %(threadName)-9s  %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

logger.debug('logger started and setup')
logger.info(' testing info')

########################################################
########################################################

############### Settings ###############################
# Select if system equipped with BMP280 board or not
__SENSOR = False



########################################################


# Function for writing to the data log file
# Takes T and P and adds a timestamp
def writeToFile(temperature_s, pressure_s, feel):

  datetimenow = datetime.datetime.now()
  longstring =( 's from epoch: ' + str(time.time()) +
                  '  Date: ' + str(datetimenow)  +
                  ', T: ' + temperature_s +
                  ', P: ' + pressure_s +
                  ', F: ' + str(feel) + '\n')

  filename = str(datetimenow.year) + '_' + str(datetimenow.month) + '_' + str(datetimenow.day) + '.csv'
  logger.debug('Using filename: %s' % filename)
  logger.debug('Writing to file: %s' % longstring)

  try:
    file = open(filename, 'a')
  except:
    logger.debug('Failed to open file')

  file.write( longstring)
  file.close()

  return

def main():
  # Extract the PORT from cli arguments
  args = sys.argv[1:]
  if not args:
    print "usage: [--PORT]";
    sys.exit(1)

  PORT = int(args[0])

  #Setting up a port and starts to listen for connections
  try:
    s = socket.socket()
  except socket.error, exc:
    print "Caught exception socket.error : %s" % exc
    sys.exit()
  # .bind wants one argument tuple , thats reason for double paranthesis
  # Set the socket reusable so that the port is released when it is killed!
  s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

  try:
    IP = '192.168.0.100'
    s.bind((IP, PORT))
  except socket.error, msg:
    print 'Exception  socket error: %s ' % msg
    sys.exit()

  t_stop= threading.Event()

  #Creating and initializing new threads for the measurements and interrups
  #Better to start threads here, and only if connection is OK

  measurementT = measureThread(1, 'measureT', t_stop)

  if __SENSOR:
    #Creat this form teh class!!
    interruptT = interrupt.interruptThread(1, 'interruptT', measurementT)

  #Starting the threads
  measurementT.daemon = True
  measurementT.start()
   # Set thread to daemon -> if main dies the thread dies
  if __SENSOR:
    interruptT.daemon = True
    interruptT.start()

  s.listen(5)

  while True:
    logger.debug('Waiting for connections')
    try:
      c, addr = s.accept()
    except KeyboardInterrupt:
      logger.debug('Recieved keyboard nterrupt in main while loop')
      #Break the while loop and try to exit
      break

    c.send('Thank you for connecting!'  +'\n')
    #Now spawn a thread with the connection
    serverT = serverThread(1, 'serverT', PORT, IP, measurementT, c, t_stop)
    # Set the new thread as daemon -> it dies if main dies
    serverT.daemon = True
    serverT.start()
    #while 1:
      #writeToFile('1','2',3)
      #time.sleep(1)

  # Out of the while loop!Try to shutdown gracefully
  logger.debug('Out of main while loop. Try to shutdown')

  #use below for now: exit(measurementT, interruptT, serverT)
  #signal threads to stop even if "sleeping"
  #Stop threads 1 by 1 as interrupts can eb cut off
  measurementT.stop()
  #Only stop teh servet thread if it exists..
  try:
    serverT
  except NameError:
    pass
  else:
    serverT.stop()

  if __SENSOR:
    interruptT.stop()

  t_stop.set()
  try:
    c
  except NameError:
    pass
  else:
    c.close()
  s.close()
  logger.debug('Last line in main')

# Function to handle the stopping of active  threads
#def exit(measurementT, interruptT, serverT):
#  measurementT.stop()
#  serverT.stop()
#  interruptT.stop()
#  return


# A class to handle interrupts from buttons
class interruptThread(threading.Thread):
  def __init__(self, threadID, name, meas):

    threading.Thread.__init__(self, )
    self.threadID = threadID
    self.name = name
    self.isRunning = True
    #link to the measurement thread
    self.myMeasurementT = meas
    #Setup listeners
    self.pd = pifacedigitalio.PiFaceDigital()
    self.listener = pifacedigitalio.InputEventListener(chip=self.pd)
    #setup listeners for all buttons
    (self.listener).register(0, pifacedigitalio.IODIR_FALLING_EDGE, self.print_flag)
    (self.listener).register(1, pifacedigitalio.IODIR_FALLING_EDGE, self.print_flag)
    (self.listener).register(2, pifacedigitalio.IODIR_FALLING_EDGE, self.print_flag)
    (self.listener).register(3, pifacedigitalio.IODIR_FALLING_EDGE, self.print_flag)
    #start listening for events (spawns a new thread->Yes!)
    logger.debug('Initiated interrupt thread: threadID %d  name  %s', self.threadID, self.name)

  def run(self):
    (self.listener).activate()
    logger.debug('Started interupt thread')

    while self.isRunning:
      #Short time loop here to help exiting when not isRunning...
      time.sleep(1)
      #logger.debug('Life sign from interrupt thread (every 5 min)')
    print 'out of interrupt while loop'
    # To kill the threads created when listeners activated, deactivate listers
    (self.listener).deactivate()

  # Function called at button interrupt. Triggers a log write of data.
  def print_flag(self, event):
    logger.debug('Button nr %d pressed', event.pin_num )
   # print 'Button nr %d pressed ' % event.pin_num
    self.myMeasurementT.write_Feel(event.pin_num)
    return

  def stop(self):
    logger.debug('Intterupt thread isRunning set False')
    self.isRunning = False
    return

#A class handling BMP280 communication
class measureThread(threading.Thread):
  def __init__(self, threadID, name, t_stop):
    threading.Thread.__init__(self)
    self.t_stop = t_stop
    self.threadID = threadID
    self.name = name
    self.timeout = time.time() + 60*60*24*60
    self.sensor = BMP280_2.BMP280()
    self.latestTemperature = 0
    self.latestPressure = 0
    self.isRunning = True

    #Need to add latest time also
    logger.debug('Initiated thread: threadID %d  name  %s', self.threadID, self.name)

  def run(self):

    #while self.isRunning:
    # Using events to exit loop even if "sleeping"
    while not self.t_stop.is_set():
      # Read sensor data and time
      temperature_f = (self.sensor).read_temperature()
      temperature_s = str(temperature_f)
      pressure_f = (self.sensor).read_pressure()
      pressure_s = str(round(pressure_f, 3))
      feel = 0 #default value
      writeToFile(temperature_s, pressure_s, feel)
      #datetimenow = str( datetime.datetime.now())
      #longstring =( 's from epoch: ' + str(time.time()) +
      #              '  Date: ' + datetimenow  +
      #              ', T: ' + temperature_s +
      #              ', P: ' + pressure_s +
      #              ', F: ' + '0' + '\n')
      #filename = str((datetime.datetime.now()).year) + '_' + str((datetime.datetime.now()).month) + '_' + str((datetime.datetime.now()).day) + '.csv'
      #logger.debug('Using filename: %s' % filename)

      #try:
      #  file = open(filename, 'a')
      #except:
      #  logger.debug('Failed to open file')

      #file.write( longstring)
      #file.close()
      self.latestTemperature = temperature_f
      self.latestPressure = pressure_f
      #logger.debug('Writing to file: %s' % longstring)

      #Wait similar to "sleep"
      self.t_stop.wait(10)

      if time.time() > self.timeout:
        break
      elif not self.isRunning:
        break

    logger.debug('Last line in Measurement thread')

  def get_T(self):
    return self.latestTemperature

  def get_P(self):
    return self.latestPressure

  def write_Feel(self, feel):
    temperature_f = (self.sensor).read_temperature()
    temperature_s = str(temperature_f)
    pressure_f = (self.sensor).read_pressure()
    pressure_s = str(pressure_f)

    writeToFile(temperature_s, pressure_s, feel)

    #All below moved to file write function. MIGHT need threaing access lock...
    #datetimenow = str( datetime.datetime.now())
    #longstring =( 's from epoch: ' + str(time.time()) +
    #             ' Date ' + datetimenow  +
    #             ', T: ' + temperature_s +
    #             ', P: '       + pressure_s +
    #             ', F: ' + str(feel) + '\n')

    #filename =(str((datetime.datetime.now()).year) + '_' +
    #      str((datetime.datetime.now()).month) + '_' +
    #      str((datetime.datetime.now()).day)  + '.csv')

    #try:
    #  file = open(filename, 'a')
    #except:
    #  logger.debug('Failed to open file')

    #file.write( longstring)
    #file.close()
    return

  def stop(self):
    logger.debug('isRunning = False')
    self.isRunning = False
    return


#Making a new thread subclass to do the server work.
class serverThread(threading.Thread):
  def __init__(self, threadID, name, port, ip, meas, c, t_stop):
    threading.Thread.__init__(self)
    self.myMeasurementT = meas
    self.threadID = threadID
    self.name = name
    self.port = port
    self.ip = ip
    self.c = c
    self.t_stop = t_stop
    self.isRunning = True
    logger.debug('Initiated thread: threadID  %d  name  %s  port %d  ip  %s',
        self.threadID, self.name, self.port, self.ip)
  def run(self):

    logger.debug('thread  name:  %s  thread ID: %d   started', self.name, self.threadID)

      #Recieve messages rom connection forever
    while self.isRunning:
      #try:
      msg = self.c.recv(1024)

      #Below important. Check if zerostring->shows connection down
      #Then close it. Kills the thread.
      if not msg:
        break
      logger.debug('Message recieved -->%s<-- (all inside arrows)', msg)

      if not self.isRunning:
        self.c.close()
        break

      #Remove string terminators
      msg = msg[:-2]
      if msg  == 'cmd1':
        logger.debug('Message cmd1 recieved' )
        self.c.send('echo cmd1'  +'\n')
      elif msg  == 'cmd2':
        logger.debug('Message cmd2 recieved' )
        self.c.send('echo cmd2'  +'\n')
      elif msg  == 'cmd3':
        logger.debug('Message cmd3 recieved' )
        self.c.send('echo cmd3'  +'\n')
      elif msg == 'get_T':
        pass
        self.c.send(str(self.myMeasurementT.get_T()) +'\n')
      elif msg == 'get_P':
        pass
        self.c.send(str(self.myMeasurementT.get_P()) + '\n')
      elif msg[:-1] == 'set_feel':
        pass
        self.myMeasurementT.write_Feel(msg[-1:])
      else:
        logger.debug('Unkonwn command recieved' )
        self.c.send('Unknown command'  +'\n')

    self.c.close()


  def stop(self):
    print 'in STOP in server Thread'
    self.isRunning = False
    return

# This is the standard boilerplate that calls the main() function.
# I think it is always good to keep it last so that all functions
# are defined before main starts
if __name__ == '__main__':
    main()
