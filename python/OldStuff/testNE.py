import json


# fh = open('cryo_describe.json', 'r')
# json_message = json.loads(fh.read())
# fh.close()


def get_input_writable(name):
     ret = raw_input('please input topic for writing to ' + name + ': ')
     return ret



""" Adds a "write_topic" and "read_topic" to the describe message.
    If methods for providing the topic string are provided those will be used,
    otherwise <equipment_id>/<module_name>/<accessible_name> for reading and .../set for writing will be used"""


def set_all_topics(describe, readable_func=None, writable_func=None):
    equipment_id = describe.get("equipment_id")
    modules = describe.get("modules")
    """Code below Collects 1 module  och e1 moduleparam klump (hoppar sen till nasta "par" i listan)"""
    for module_name, module in zip(modules[0::2], modules[1::2]):
        parameters = module.get("parameters")
        for parameter_name, parameter in zip(parameters[0::2], parameters[1::2]):
            if not parameter.get("readonly"):
                if readable_func:
                    parameter["write_topic"] = readable_func(parameter_name)
                else:
                    parameter["write_topic"] = equipment_id + '/' + module_name + '/' + parameter_name
            else:
                if writable_func:
                    parameter["write_topic"] = writable_func(parameter_name)
                else:
                    parameter["write_topic"] = equipment_id + '/' + module_name + '/' + parameter_name + '/set'


def set_writable_topics(describe, func=None): #The "none" allows no argument to be passed and use default instead
    equipment_id = describe.get("equipment_id")
    modules = describe.get("modules")
    #print modules
    print "gurkooooor"
    for module_name, module in zip(modules[0::2], modules[1::2]): #This shit picks out every second in the array!!(e.g. the module names..)
        parameters = module.get("parameters")
        for parameter_name, parameter in zip(parameters[0::2], parameters[1::2]):
            if not parameter.get("readonly"):
                if func:
                    parameter["write_topic"] = func(parameter_name)
                else:
                    parameter["write_topic"] = equipment_id + '/' + module_name + '/' + parameter_name + '/set'


def set_readable_topics(describe, func=None):
    equipment_id = describe.get("equipment_id")
    modules = describe.get("modules")
    for module_name, module in zip(modules[0::2], modules[1::2]):
        parameters = module.get("parameters")
        for parameter_name, parameter in zip(parameters[0::2], parameters[1::2]):
            if parameter.get("readonly"):
                if func:
                    parameter["write_topic"] = func(parameter_name)
                else:
                    parameter["write_topic"] = equipment_id + '/' + module_name + '/' + parameter_name




# set_writable_topics(json_message, get_input_writable)
#set_writable_topics(json_message, get_input_writable)
#print 'json'
#print json.dumps(json_message, sort_keys=True, indent=4,separators=(',', ': '))
