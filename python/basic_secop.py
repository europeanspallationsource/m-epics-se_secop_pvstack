import json

nodes = {}

""" This is a test implementation of SECoP, it will be written more for understanding SECoP than usefulness
    This implementation will be according to:
    https://github.com/SampleEnvironment/SECoP/blob/master/protocol/secop_v2018-10-04.rst

    PROPERTIES - are the properties belonging to each class
    COMMANDS - are the commands related to each class. The command that acts on that object.

    """

# ____________________________________________________________________________________________
# ____________________________________ACCESSIBLE______________________________________________
# ____________________________________________________________________________________________




class Accessible(object):
    """
    ____PROPERTIES____:

    description
        mandatory string describing the accessible, formatted as for module-description or node-description

    readonly
        mandatory boolean value indiciation if this parameter may be changed by an ECS, or not

    datatype
        mandatory datatype of the accessible, see Data Types.
        This is always a JSON-Array containing at least one element: a string naming the datatype.
        note: commands and parameters can be distinguisehd by the datatype.

    unit
        optional string giving the unit of the parameter.
        (default: unitless, SHOULD be given, if meaningfull, empty string: unit is one)
        Only SI-units (including prefix) SHOULD be used for SECoP units preferrably.

    visibility
        the visibility of the accessible. values and meaning as for module-visibility above.
        remark: this 'inherits' from the module property.
        i.e. if it is not specified, the value of the module-property (if given) should be used instead

    group
        optional identifier, may contain ":" which may be interpreted as path separator.
        The ECS may group the parameters according to this property.
        The lowercase version of a group must not match any lowercase version of an accessible name of the same module.
        (see: SECoP Issue 8: Groups and Hierarchy)
        remark: the parameter-property ``group`` is used for grouping of parameters within a module,
        the module-property ``group`` is used for grouping of modules within a node.
        remark: commands do not have ``readonly`` and ``unit`` properties, as they make no sense for commands.


    ___COMMANDS___:

    Change Value
        the change value message contains the name of the module or parameter and the value to be set.
        The value is JSON formatted. As soon as the set-value is read back from the hardware, all clients,
        having activated the parameter/module in question, get an "update" message.
        After all side-effects are communicated, a "changed" reply is then send,
        containing a Data report of the read-back value.
        remarks:
            If the value is not stored in hardware, the "update" message can be sent immediately.*
            The read-back value should always reflect the value actually used.*
            an client having activated updates may get an update message before the changed message,
            both containing the same data report.

        Example on a connection with activated updates. Qualifiers are replaced by {...} for brevity here.

        > read mf:status
        < update mf:status [[100,"OK"],{...}]
        > change mf:target 12
        < update mf:status [[300,"ramping field"],{...}]
        < update mf:target [12,{...}]
        < changed mf:target [12,{...}]
        < update mf:value [0.01293,{...}]

        The status changes from "idle" (100) to "busy" (300). The ECS will be informed with a further update message on
        mf:status, when the module has finished ramping. Until then, it will get regular updates on the current
        main value (see last update above).
        note: it is vital that all 'side-effects' are realised (i.e. stored in internal variables) and be communicated,
        before the 'changed' reply is sent!

    Read Request
        With the read request message the ECS may ask the SEC node to update a value as soon as possible,
        without waiting for the next regular update. The reply is an update message. If updates are not activated,
        the update message can also be treated like a reply request to the read request.

        Example:
        > read t1:value
        < update t1:value [295.13,{"t":1505396348.188}]
        > read t1:status
        > update t1:status [[100,"OK"],{"t":1505396348.548}]

        remark: If a client has activated the module/parameter for which it sent a ``read`` request,
        it may receive more than one 'update' message, especially if SEC node side polling is active.
        There is no indication, which message was sent due to polling (or other clients requesting a 'read') and or due
        to a specific read. An ECS-client may just use the first matching message and treat it as 'the reply'.

    Execute Command
        If a command is specified with an argument, the actual argument is given in the data part as a json-text.
        This may be also a json-object if the datatype of the argument specifies that (i.e. the type of
        the single argument can also be a struct, tuple or an array, see data types).
        The types of arguments must conform to the declared datatypes from the datatype of the command argument.

        A command may also have a return value, which may also be structured. The "done" reply always contains
        a Data report with the return value. If no value is returned, the data part is set to "null".
        The "done" message should be returned quickly, the time scale should be in the order of the time needed for
        communications. Still, all side-effects need to be realised and communicated before.
        Actions which have to wait for physical changes, can be triggered with a command, but not be waited upon.
        The information about the duration and success of such an action has to be transferred via the status parameter.

        Important
            If a command does not require an argument, the argument SHOULD be transferred as json-null. A SEC node MUST
            also accept the message, if the data part is emtpy and perform the same action.

        Example:
        > do t1:stop
        < done t1:stop [null,{"t":1505396348.876}]
        > do t1:stop null
        < done t1:stop [null,{"t":1505396349.743}]
        """

    def __init__(self, name, module, description, readonly, datatype, unit="", visability=None, group=None):
        self.name = name
        self.description = description
        self.readonly = readonly
        self.datatype = datatype
        self.unit = unit
        self.visability = visability
        self.group = group
        self.value = None
        self.belongs_to = module

    def change(self, value):
        if not self.readonly:
            self.value = value
            return True
        else:
            return False

    def read(self):
        return self.value

# ____________________________________________________________________________________________
# ____________________________________MODULE__________________________________________________
# ____________________________________________________________________________________________




class Module(object):
    """
    ____PROPERTIES____:

    description
        mandatory text describing the module, formatted like the node-property description

    visibility
        optional string indicating a hint for UI's for which user roles the module should be display or hidden.
        MUST be one of "expert" (3), "advanced" (2) or "user" (1) (default).
        Note: this does not imply that the access is controlled.
        It is just a hint to the UI for the amount of exposed modules.
        A visibility of "advanced" (2) means that the UI should hide the module for users,
        but show it for experts and advanced users.

    interface_class
        mandatory list of matching classes for the module, for example ["Magnet", "Drivable"]
        note: as this is a list it SHOULD actually have been called ``interface_classes`` or ``interfaces``

    features
        optional list of features for the module,
        for example ["HasRamp", "HasTolerance"] this is not yet part of the standard.

    group
        optional identifier, may contain ":" which may be interpreted as path separator.
        The ECS may group the modules according to this property.
        The lowercase version of a group must not match any lowercase version of a module name on the same SEC node.
        (see: SECoP Issue 8: Groups and Hierarchy)

    meaning
        optional tuple, with the following two elements:
            a string from an extensible list of predefined meanings:
                'temperature' (the sample temperature)
                'temperature_regulation' (to be specified only if different from 'temperature')
                'magneticfield'
                'electricfield'
                'pressure'
                'rotation_z' (counter clockwise when looked at 'from sky to earth')
                'humidity'
                'viscosity'
                'flowrate'
                'concentration'

            This list may be extended later. (see: SECoP Issue 26: More Module Meanings).
            '_regulation' may be postfixed, if the quantity generating module is different from the
            (closer to the sample) relevant measuring device.
            A regulation device MUST have an interface_class of at least Writable.

            a value describing the importance, with the following values:
                10 means the instrument/beamline (Example: room temperature sensor always present)
                20 means the surrounding sample environemnt (Example: VTI temperature)
                30 means an insert (Example: sample stick of dilution insert)
                40 means an addon added to an insert (Example: a device mounted inside a dilution insert)

            Intermediate values might be used. The range for each category starts at the indicated value minus 5 and
            ends below the indicated value plus 5. (see also: SECoP Issue 9: Module Meaning)

    ___COMMANDS___:

    Only extended messages acts on the module, for later...

"""

    def __init__(self, name, description, interface_class, visibility=None, features=None, group=None, meaning=()):
        self.name = name
        self.description = description
        self.interface_class = interface_class
        self.visibility =visibility
        self.features = features
        self.group = group
        self.meaning = meaning
        self.accessibles = []
        self.belongs_to = node
        self.structure_report = {}


    def describe(self):
        self.structure_report = {"description": self.description, "interface_class": self.interface_class}
        if self.features:
            self.structure_report["features"] = self.features
        if self.visibility:
            self.structure_report["visibility"] = self.visibility


# __________________________________________________________________________________________
# ______________________________________NODE__________________________________________________
# ____________________________________________________________________________________________

class Node(object):
    """
    ____PROPERTIES____:

    equipment_id
        mandatory, worldwide unqiue id of an equipment as string.
        Should contain the name of the owner institute or provider company as prefix in order
        to guarantee worldwide uniqueness.
        example: "MLZ_ccr12" or "HZB-vm4"

    description
        mandatory text describing the node, in general. The formatting should follow the 'git' standard,
        i.e. a short headline (max 72 chars), followed by \n\n and then a more detailed description,
        using \n for linebreaks.

    firmware
        optional, short, string naming the version of the SEC node software.
        example: frappy-0.6.0

    timeout
        optional value in seconds, a SEC node should be able to respond within a time well below this value.
        (i.e. this is a reply-timeout.) Default: 10 se

    ___COMMANDS___:

    Identification
        The syntax of the identification message differs a little bit from other messages,
        as it should be compatible with IEEE 488.2. The identification request "*IDN?" is meant to be sent
        as the first message after establishing a connection. The reply consists of 4 comma separated fields,
        where the second and third field determine the used protocol.
        In this and in the following examples, messages sent to the server are marked with "> ",
        and messages sent to the client are marked with "< "

        Example:
            > *IDN?
            < ISSE&SINE2020,SECoP,V2018-10-04,rc2

        So far the SECoP version is given like "V2018-10-04", i.e. a capital "V" followed by a date in year-month-day
        format with 4 and 2 digits respectively.
        The add.info field is used to differentiate between draft, release candidates (rc1, rc2,...) and final.

    Description
        The next messages normally exchanged are the description request and reply. The reply contains
        the Structure report i.e. a structured JSON object describing the name of modules exported and their parameters,
        together with the corresponding properties.

        Example:
            > describe
            < describing . {"modules":["t1",["interface_class",["TemperatureSensor","Readable"],"accessibles",["valu...

        The dot (second item in the reply message) is a placeholder for extensibility reasons. A client implementing
        the current specification MUST ignore it.
        Remark: this reply might be a very long line, no raw line breaks are allowed in the JSON part! I.e.
        the JSON-part should be as compact as possible.

    Activate Updates
        The parameterless "activate" request triggers the SEC node to send the values of all its modules and parameters
        as update messages (initial updates). When this is finished, the SEC node must send an "active" reply. (global
        activation)
        note: the values transferred are not necessarily read fresh from the hardware, check the timestamps!
        note: This initial update is to help the ECS establish a copy of the 'assumed-to-be-current' values
        A SEC node might accept a module name as second item of the message (module-wise activation), activating only
        updates on the parameters of the selected module. In this case,
        the "active" reply also contains the module name. A SEC Node not implementing module-wise activation MUST NOT
        send the module name in its reply to an module-wise activation request,
        and MUST activate all modules (fallback mode).
        remark: This mechanism may be extended to specify modulename:parametername for a parameter-wise activation.
        A SEC node capable of module-wise activation SHOULD NOT fallback to global activation if it encounters such a
        request. Instead it SHOULD fallback to module-wise activation,
        i.e. ignore anything after (including the) colon in the specifier.

    Update
        When activated, update messages are delivered without explicit request from the client.
        The value is a Data report, i.e. a JSON array with the value as its first element,
        and an JSON object containing the Qualifiers as its second element.
        An update may also be triggered by an read request, in which case the value reported in the data report is
        fresh (i.e. just obtained from a hw).

        Example:
            > activate
            < update t1:value [295.13,{"t":150539648.188388,"e":0.01}]
            < update t1:status [[400,"heater broken or disconnected"],{"t":1505396348.288388}]
            < active
            < update t1:value [295.14,{"t":1505396349.259845,"e":0.01}]
            < update t1:value [295.13,{"t":1505396350.324752,"e":0.01}]

        The example shows an activate request triggering an initial update of two values: t1:value and t1:status,
        followed by the active reply. After this two more updates on the t1:value show up after roughly 1s between each.

    Deactivate Updates
        A parameterless message. After the "inactive" reply no more updates are delivered if not triggered by a read
        message.

        Example:
            > deactivate
            < update t1:value [295.13,{"t":1505396348.188388}]
            < inactive

        remark: the update message in the second line was sent before the deactivate message was treated. After the
        "inactive" message, the client can expect that no more untriggered update message are sent, though it MUST
        still be able to handle (or ignore) them, if they still occur.
        The deactivate message might optionally accept a module name as second item of the message for module-wise
        deactivation. If module-wise deactivation is not supported, it should ignore a deactivate message which contains
        a module name.
        Remark: it is not clear, if module-wise deactivation is really useful.
        A SEC Node supporting module-wise activation does not necessarily need to support module-wise deactivation.

    """

    def __init__(self, equipment_id, description, firmware=None, timeout=10):
        self.equipment_id = equipment_id
        self.description = description
        self.firmware = firmware
        self.timeout = timeout
        self.modules = []
        self.structure_report = {}

    def describe(self):
        self.structure_report = {"equipment_id": self.equipment_id, "description": self.description}
        if self.firmware is not '':
            self.structure_report["firmware"] = self.firmware
        if self.timeout is not '':
            self.structure_report["timeout"] = int(self.timeout)

    def add_module(self, node, json_str):
        module_dict = json.loads(json_str)
        module = Module(module_dict.get('module_name'), module_dict.get('description'),
                        module_dict.get('interface_class'), module_dict.get('visibility'),
                        module_dict.get('features'), module_dict.get('group'))
        node.modules.append(module)

# HERE


def add_node(json_str):
    node_dict = json.loads(json_str)
    node = Node(node_dict.get('equipment_id'), node_dict.get('description'), node_dict.get('firmware'),
                node_dict.get('timeout'))
    nodes.update({node_dict.get('equipment_id'): node})
    node.describe()


def get_node_list():
    return list(nodes.keys())


if __name__ == "__main__":

    add_node('{"equipment_id": "test2", "description": "testin node", "firmware": "1234", "timeout": ""}')
    add_node('{"equipment_id": "test3", "description": "testing node", "firmware": "4321", "timeout": "30"}')
    nodes.get('test2').add_module({"module_name": "test_module", "description": "cesar", "interface_class": "bengt",
                                   "timeout": "", "features": "", "group": ""})
    print(nodes.get('test2').structure_report)
    print(nodes.get('test2').timeout)

"""
    name = input('Name of node: ')
    description = input('Human readable description of node: ')
    firmware = input('firmware')
#    node =
    node = Node('test01', 'testing node\n\nNode for testing stuff', 'test_firmware_01')

    node.describe()
    readonly = True
    print(node.structure_report)
    if isinstance(readonly, bool):
        print('hej')
"""