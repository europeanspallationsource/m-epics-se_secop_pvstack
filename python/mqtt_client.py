import paho.mqtt.client as mqtt
import basic_config #AP version of basic_secop. To build config object
import json
import re


##########AP Development###########################

#Reuse function from NE development
#from python.testNE import set_writable_topics, get_input_writable
import basic_config
#Import my own crap
import configuration
##

#To find path to current folder
import os



#import proper libraries
#import json
#import glob #For listing files in dir.
######################################################




MQTT_host = "10.4.0.53" #"192.168.1.25"
MQTT_port = "1883"
"""
<self.topic>
    |
    ---/node
    |    |
    |    ------/new     self.on_new_node_message()    <-
    |
    ---/nodes           nodes[]                     ->
          |
          ----/#        self.on_node_message()      <-
    

"""

class mqtt_client(object):
    def __init__(self, host, port):
        self.topic = 'octopus'
        self.mqtt_host = host
        self.mqtt_port = port
        self.client = mqtt.Client()
        self.client.will_set(self.topic + '/nodes', '', retain=True)
        self.client.connect(self.mqtt_host, int(self.mqtt_port), 60,)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.node = None
        self.add_callbacks()
        self.client.loop_forever()
        #AP adding
        self.activeconfig
        self.currentconfigurationfile
        self.savename

    def add_callbacks(self):
        self.client.message_callback_add(self.topic + '/node/new', self.on_new_node_message)
        self.client.message_callback_add(self.topic + '/nodes/+', self.on_nodes_message)
        self.client.message_callback_add(self.topic + '/configuration/+', self.on_configuration_message)
        print('added callback')

    def on_new_node_message(self, client, userdata, msg):
        print('new node')
        self.node = basic_secop.add_node(msg.payload.decode('utf-8'))
        self.publish_nodes()

    def on_nodes_message(self, client, userdata, msg):
        print(msg.topic)
        match = re.match(self.topic + '\x2F?nodes\x2F?(?P<node_name>[a-zA-Z\d]*)', msg.topic)
        if match:
            node_name = match.group('node_name')
            print('node name: ' + node_name)
            if node_name:
                if msg.payload == b'describe':
                    self.client.publish(self.topic + 'nodes/' + node_name,
                                        json.dumps(basic_secop.nodes.get(node_name).structure_report))
            else:
                print('unknown command')
        else:
            print('no nodes by that name')


    # Reading a configuration file (selected in GUI currently) and creating a Config object
    def on_configuration_message(self, client, userdata, msg):
        print(msg.topic)
        #Start. activate the current selected configuration
        if "start" in msg.topic:
            print "hej"
            # Section for finding the local dir where the py is running.
            # Then read the selected file name (selected in node-red) ##
            path = os.path.dirname(os.path.abspath(__file__))
            #Add the stored configuration. Shoudl probably be in a config object..
            #Create absolut path
            path = path + "/" + self.currentconfigurationfile
            print "Absolute file path:   " + path
            fh = open(path, 'r+')
            json_message = json.loads(fh.read())
            fh.close()
            print "Skipping printing the full Json as pretty print.Can remove comments to get print "
            #print 'Dumping the file in Json format with pretty print:' #Pretty print the json from file
            #print json.dumps(json_message, sort_keys=True, indent=4, separators=(',', ': '))
            ###### END of secton reading in teh json from file ################################


            #CREATING A NODE++++++++++++++++++++++++++++++++++++++++++
            self.activconfig = basic_config.add_node(json.dumps(json_message))
            print "Print of response from configuration structure_report function"
            print self.activconfig.structure_report #test the report function


            #EXTRACTIING TEH MODULES IN TEH DESCRIBE
            modules_list = json_message.get("modules")
            #for module in modules_array[]
            print "Print modules "
            # Create Modules.
            # Loop through the modules (every 2nd item in the list is "module name"  and "module object"!
            for module_name_str, module_obj in zip(modules_list[0::2], modules_list[1::2]):
                module_name = module_name_str
                print module_obj
                #print "the arrau"
                #print module_obj.get("commands")
                group = module_obj.get("group")
                description = module_obj.get("description")

                #print module_obj.get("_implementation")
                visibility = module_obj.get("visibility")
                features = module_obj.get("features")
                interface_class = module_obj.get("interface_class")
                _implementation = module_obj.get("_implementation")



                # add_module2 used below is AP version of the add_module with differnt args
                module =self.activconfig.add_module2(self.activconfig, module_name, description, interface_class, _implementation,  visibility, features, group)

                #adding the accessible for this module

                #Trying to do  mixture of adding all as accesibles, BUT still keep the split between PArameters and Commands
                parameters_list = module_obj.get("parameters") #Extracting the accesibles (still parameters here) as a list (?)

                # Loop through the list in pairs (every 2nd item in the list is "name string"  and "parameter object"!
                for parameter_str, parameter_obj  in zip(parameters_list[0::2], parameters_list[1::2]):
                    print parameter_str + "      " + str(parameter_obj)
                    name = parameter_str
                    description = parameter_obj.get("description")
                    readonly = parameter_obj.get("readonly")
                    datatype = parameter_obj.get("datatype")
                    unit = parameter_obj.get("unit")
                    visability = parameter_obj.get("visability")
                    group = parameter_obj.get("group")
                    link = parameter_obj.get("link")

                    #This below shoudl replace teh one further below
                    self.activconfig.add_parameter(name, module, description, readonly, datatype, unit, visability,
                                                     group, link)
                    # Calling the AP version (ends with 2) of the add_accessible function for each accesible.
                    self.activconfig.add_accessible2(name, module, description, readonly, datatype, unit, visability,
                                            group)
                #Same procedure for commands as for parameters above
                commands_list = module_obj.get("commands")
                for command_str, command_obj in zip(commands_list[0::2], commands_list[1::2]):
                    print command_str + "      " + str(command_obj)
                    name = command_str
                    # module = parameter_obj #redudant?
                    description = command_obj.get("description")
                    readonly = command_obj.get("readonly")
                    datatype = command_obj.get("datatype")
                    unit = command_obj.get("unit")
                    visability = command_obj.get("visability")
                    group = command_obj.get("group")
                    # Calling the AP version (ends with 2) of the add_accessible function for each accesible.
                    self.activconfig.add_command(name, module, description, readonly, datatype, unit, visability, group)


            #Below just a testprint for testing teh function listaAccesibles of a module.
            print "Printing all the accesibles of module index 0 "
            (self.activconfig.modules[0]).listAccessibles()

        elif "edit" in msg.topic: #sstor currently selected name for confilg file
            print "edit"
            #Fetch a node object (take first in list for now..)
            node = (basic_config.get_node_list())[0]
            accessibles = node.getaccesibles() #A useful function i think?
            print (node.modules[0].parameters)




        elif "savename" in msg.topic: #sstor currently selected name for confilg file
            print "savename"
            if "txt" in msg.payload:
                self.savename = msg.payload
            else:
                self.savename = str(msg.payload) + ".txt"
            print self.savename

        elif "updatelist" in msg.topic:  # stores currently selected name for config file
            print "updatefilelist"
            #Get current directory path
            directorypath = os.getcwd()
            #Get the files in that directory
            filelist = os.listdir(directorypath)
            templist=filelist
            # Sort out the config files (containing _config and  .txt )
            for file in list(filelist):
                if (False == ("_config" in file) or False == (".txt" in file)   ):
                    templist.remove(file)
            filelist = templist
            # Print the list of files in the current directory
            print filelist  # returns list
            #Publishing the list of available files
            self.client.publish(self.topic + '/configuration/filelist',
                                json.dumps(filelist))


        elif "save" in msg.topic: #save the currently selected configuration to file as a config file
            print "Saving configuration: " + self.currentconfigurationfile + "   to file : " + self.savename
            #Call the returnnode function. Returns a describe message from the current configuration
            node = self.activconfig.returnnode()
            print node


        elif "current" in msg.topic: #set the currently selected configuration
            self.currentconfigurationfile = msg.payload
            print "setting current configuration file selected to:  " + self.currentconfigurationfile
        else:
            print('no configuration function by that name')



    def on_describe_node_message(self, client, userdata, msg):
        print('describe node')
        node_name = msg.payload.decode('utf-8')
        self.client.publish(self.topic + 'nodes/' + node_name,
                            json.dumps(basic_secop.nodes.get(node_name).structure_report))

    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))
        client.subscribe(self.topic + "/#")

    def publish_nodes(self):
        print('publish nodes')
        self.client.publish(self.topic + '/nodes', json.dumps(list(basic_secop.nodes.keys())), retain=True)
#        for node in basic_secop.nodes:
#            print(node.structure_report)
#            self.client.publish(self.topic + '/nodes', json.dumps(node.structure_report))

    def mqtt_error(self, error_message):
        self.client.publish(self.topic + "/" + "error", error_message)

    def on_message(self, client, userdata, msg):
            val = msg.payload.decode('utf-8')
            print(val)


client = mqtt_client(MQTT_host, MQTT_port)

